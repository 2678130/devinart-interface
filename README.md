# Devinart Interface
## State management
### State management can be used with the custom hook *useStore*

1. To create a new store we pass a function and an initial value, for example we track 10 cats in our cat clowder (group of cats)...
``` javascript
import {useStore} from '../customHooks';
const [cats, setCats] = useStore('cats', 10);
```

2. If one of our component wants to know how many cats are in our cat clowder we can call useStore anywhere...
``` javascript
import {useStore} from '../customHooks';
const [cats, setCats] = useStore('cats');

<h2> We have {cats} in our cat clowder </h2>
```

3. If one of our cat leaves our cat clowder we can change its value using setCats and all components using cats will be updated...

``` javascript
import {useStore} from '../customHooks';
const [cats, setCats] = useStore('cats');

const catLeavesClowder = () => {
    setCats(9);
}
```

### Using modals
1. To use a modal
``` javascript
import React, {useState} from 'react';
import Modal from '../Modal';

// Following code in a React Component

const [open, setOpen] = useState(false);

// Modal props must implement open and onClose
<button onClick={ () => { setOpen(true)} }>Open Modal</button>
{ open &&
<Modal open={ open } onClose={ () => { setOpen(false); } }>
    {// Component or jsx here
    }
</Modal>
}
```


### App Store
``` javascript

const player = {
    name,
    id,

}


const state = {
    host<bool> : false,
    avatar<number> : 0,
    accessCode<string>: 'ABC12',


}

const draw = {

}

const roundInfo = {
    creator<string> : 'someId',
    
}




```


Iphone 8 screen size 414x715
import React, { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import { pages } from "#pages/pages";
import { useDetectMobile } from "#hooks"
import "./Background.css";
import { useSpring, animated } from 'react-spring';
import scribble_bulb from "#imgs/scribbles/scribble-bulb.png";
import scribble_camp from "#imgs/scribbles/scribble-camp.png";
import scribble_paperplane from "#imgs/scribbles/scribble-paperplane.png";
import scribble_RV from "#imgs/scribbles/scribble-RV.png";
import scribble_camping_trees from "#imgs/scribbles/camping-trees.png";
import scribble_hot_air_ballon from "#imgs/scribbles/hot-air-balloon.png";
import scribble_jar from "#imgs/scribbles/jar.png";
import scribble_moon_camping from "#imgs/scribbles/moon-camping.png";
import scribble_mountain from "#imgs/scribbles/mountain.png";
import scribble_penguin from "#imgs/scribbles/penguin.png";
import scribble_space_bulb from "#imgs/scribbles/space-bulb.png";
import scribble_trible from "#imgs/scribbles/trible.png";

export const Background = () => {
    const currentPath = useLocation().pathname;
    const isMobile = useDetectMobile();
    let style = {};

    if (!isMobile) style = {
        borderRadius: '5px',
        boxShadow: '0 0 12px 6px rgba(0,0,0,0.15)'
    };



    const positions = {
        [pages.landing.path]: {
            x: -410,
            y: -200,
            img1_x: 55,
        },
        [pages.waitingRoom.path]: {
            x: 180,
            y: -300,
        },
        other: {
            x: 180,
            y: -500,
        }
    };


    let initialPosition = positions[currentPath] || positions.other;
    const [props, set] = useSpring(
        () => ({
            xy: [initialPosition.x, initialPosition.y],
            config: {
                tension: 200,
                mass: 3,
                // friction: 26,
                // duration: 300,
            }
        })
    )
    const transform = (x, y) => `translate(${x}px, ${y}px )`;






    useEffect(() => {
        // animation translation
        let currentPosition = positions[currentPath] || positions.other;
        set({ xy: [currentPosition.x, currentPosition.y] });
        return () => {
        }
    }, [currentPath])

    return (
        <div className="default-background" style={style}>
            <div className="default-background-inner">
                <animated.div className="circle" style={{ transform: props.xy.interpolate(transform) }}>
                    <div className="scribble" style={{ right: "3rem", bottom: "21rem", transform: "rotate(55deg)" }}>
                        <img className="shadowed bulb" src={scribble_bulb} alt="scribble" />
                    </div>
                    <div className="scribble" style={{ right: "2rem", bottom: "36rem", transform: "rotate(20deg)", width: "8em" }}>
                        <img className="shadowed camp" src={scribble_camp} alt="scribble" />
                    </div>
                    <div className="scribble" style={{ right: "14.5rem", bottom: "44rem", transform: "rotate(-40deg)", width: "9em" }}>
                        <img className="shadowed RV" src={scribble_RV} alt="scribble" />
                    </div>
                    <div className="scribble" style={{ right: "17rem", bottom: "33.5rem", transform: "rotate(60deg)" }}>
                        <img className="shadowed paperplane" src={scribble_paperplane} alt="scribble" />
                    </div>
                    <div className="scribble" style={{ right: "16rem", bottom: "21.5rem", transform: "rotate(70deg)" }}>
                        <img className="shadowed camping-trees" src={scribble_camping_trees} alt="scribble" />
                    </div>
                    <div className="scribble" style={{ right: "35.5rem", bottom: "28.5rem", transform: "rotate(-20deg)" }}>
                        <img className="shadowed jar" src={scribble_jar} alt="scribble" />
                    </div>
                    <div className="scribble" style={{ right: "26rem", bottom: "4rem", transform: "rotate(25deg)" }}>
                        <img className="shadowed moon-camping" src={scribble_moon_camping} alt="scribble" />
                    </div>
                    <div className="scribble" style={{ right: "25.5rem", bottom: "27rem", transform: "rotate(25deg)" }}>
                        <img className="shadowed mountain" src={scribble_mountain} alt="scribble" />
                    </div>
                    <div className="scribble" style={{ right: "11.5rem", bottom: "5.5rem", transform: "rotate(25deg)" }}>
                        <img className="shadowed trible" src={scribble_trible} alt="scribble" />
                    </div>
                    <div className="scribble" style={{ right: "36.5rem", bottom: "15.5rem", transform: "rotate(-40deg)" }}>
                        <img className="shadowed space-bulb" src={scribble_space_bulb} alt="scribble" />
                    </div>
                    <div className="scribble" style={{ right: "27.5rem", bottom: "15.5rem", transform: "rotate(20deg)" }}>
                        <img className="shadowed hot-air-balloon" src={scribble_hot_air_ballon} alt="scribble" />
                    </div>
                </animated.div>
            </div>
        </div>
    )
}
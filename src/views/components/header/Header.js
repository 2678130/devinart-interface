import React from 'react';
import "./Header.css";
import { useWebsocket, useStore } from "#hooks";
import { useHistory, useLocation } from 'react-router-dom';
import { pages } from "#pages/pages";
import { BackButton } from '#ui/inputs';

export default function Header() {
    const path = useLocation().pathname;
    const [player] = useStore('player');
    const history = useHistory();
    const paths = generatePathArray();
    const { emitWebsocket } = useWebsocket();

    function generatePathArray() {
        const tempPaths = [];
        for (let p in pages) {
            tempPaths.push(pages[p].path)
        }
        return tempPaths
    }

    const handleRedirect = (event) => {
        const index = paths.indexOf(path);

        // case pregame
        if (index < 3) {
            history.push(paths[Math.max(index - 1, 0)]);
            // case lobby 
            if (index === 2) {
                emitWebsocket('game:leave', player);
            }
            return;
        }


        // case Game pages
        // Alert
        // leave game
        history.push(paths[2]);
    }

    // Landing page
    if (path === pages.landing.path) {
        return (
            <>
            </>
        )
    }

    return (
        <header className="default-header">
            <BackButton onClick={handleRedirect} />
        </header>
    )
}
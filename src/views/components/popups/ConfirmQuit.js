import React from 'react';
import {ButtonYesNo} from '../../../components/inputs/Button';
import { useHistory } from "react-router-dom";
import {pages} from '../../pages/pages';
import "./ConfirmQuit.css";

export default function ConfirmQuit({onClose}) {
    const history = useHistory();
    const action = {oui: "Oui", non: "Non"}
    const handleClick = (event) =>{
        const {name} = event.target;
        if(name === action.oui){
            onClose();
        }
        if(name === action.non){
            history.push(pages.createUser.path);
        }
    }
    return (
        <div className="default-confirm-quit">
            <div>Voulez-vous quitter la partie?</div>
            <div className="confirm-quit-buttons">
                <ButtonYesNo name={action.oui} onClick={handleClick}/>
                <ButtonYesNo name={action.non} onClick={handleClick}/>
            </div>
        </div>
    )
}

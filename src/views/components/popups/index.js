import ChooseWord from './ChooseWord';
import ConfirmQuit from './ConfirmQuit';

export {
    ChooseWord,
    ConfirmQuit,
}
import React from 'react';
import "./Footer.css";
import { useDetectMobile, useStore } from "#hooks"
import { useHistory, useLocation } from 'react-router-dom';
import { pages } from "#pages/pages";
import {ButtonSettings, ButtonQuestionMarkBlue } from "#ui/inputs";

export default function Header() {
    const isMobile = useDetectMobile();
    const path = useLocation().pathname;
    const history = useHistory();
    const [accessCode] = useStore('accessCode');

    const handleRedirect = () =>{
        history.push(pages.waitingRoom.path);
    }
    // Web version
    if (!isMobile) {
        // Web rendering here
    }

    // Mobile version

    // Landing page
    if (path === pages.landing.path || path === pages.guessingPage.path || path === pages.creatorPage.path){
        return (
            <>
            </>
        )
    }
    else if (path === pages.waitingRoom.path){
        return (
            <footer className="default-footer">
                <div>
                    <ButtonSettings />
                </div>
                <div className="access-code">
                    <span>{accessCode}</span><br/><span className="sous-mot-code">Code d'accès</span>
                </div>
                <div>
                    <ButtonQuestionMarkBlue />
                </div>
            </footer>
        )
    }

    return (
        <footer className="default-footer">
            <div>
                <ButtonSettings />
            </div>
            
            <div>
                <ButtonQuestionMarkBlue />
            </div>
        </footer>
    )
}
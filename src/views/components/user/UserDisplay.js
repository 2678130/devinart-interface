import React from 'react';
import "./UserDisplay.css";
import logo from '../../../imgs/avatar/avatar-1.png'
import { useStore } from '#hooks';
import Avatar from './Avatar';

export default function UserDisplay({ name, master, avatar, index, points }) {
    // du code ici, timer start
    // <img id="joueur-avatar" src='../logo192.png'>Avatar</img>
    const [player] = useStore('player')

    // Changer le nom de classe pour quelque chose de plus significatif

    // Fonction qui ajoute un boutton kick si master = true mais pas de boutton si master = false

    // Ajouter le nom du joueur dans le rectangle
    let colorCSSArray = ["blue-avatar", "green-avatar", "purple-avatar", "yellow-avatar", "pink-avatar", "orange-avatar", "red-avatar"];
    let colorCSSArrayCercle = ["blue-avatar-cercle", "green-avatar-cercle", "purple-avatar-cercle", "yellow-avatar-cercle", "pink-avatar-cercle", "orange-avatar-cercle", "red-avatar-cercle"];
    let cssString = `user-display ${colorCSSArray[index % 7]}`;
    let cssString2 = `user-display ${colorCSSArrayCercle[index % 7]}`;
    name = name.substring(0, 7);
    
    return (
        <div className={cssString}>
            <div className="avatar-name-div">
                <Avatar index={avatar} width={2.5}/>
                <div className="user-display-player-name">{name}</div>
            </div>
            { 
                (player.host && points === undefined) &&
                <div><button className="user-display-kick-btn">{master}Kick</button></div>
            }
            {
                (points !== undefined) && 
                <div className="white-box-points">
                    <div className={cssString2}>{index + 1}</div>
                    <div className="user-resultat-points">{points} pts</div>
                </div>
            }
        </div>
    )
}


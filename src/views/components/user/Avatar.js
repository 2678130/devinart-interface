import styled from 'styled-components';
import avatar1 from '#imgs/avatar/avatar-1.png';
import avatar2 from '#imgs/avatar/avatar-2.png';
import avatar3 from '#imgs/avatar/avatar-3.png';
import avatar4 from '#imgs/avatar/avatar-4.png';
import avatar5 from '#imgs/avatar/avatar-5.png';
import avatar6 from '#imgs/avatar/avatar-6.png';

const Avatar = ({index, onClick, selected, width}) => {
    index = index || 0;
    selected = selected || false;
    width = width || 6;

    const avatars = [avatar1, avatar2, avatar3, avatar4, avatar5, avatar6];


    return (
        <AvatarWrapper onClick={onClick} name={index} selected={selected} width={width}>
            <img src={avatars[index]} alt="avatar" style={{ height: '100%' }} />
        </AvatarWrapper>
    )
}

const AvatarWrapper = styled.div`
    height: ${props => props.width}rem;
    cursor: ${props => props.onClick === undefined ? 'arrow' : 'pointer'};
    box-shadow: ${props => props.selected ? '0 0 6px 6px rgba(0, 0, 0, 0.2)' : 'none'};
    border-radius: 999px;
`


export default Avatar
import React, { useState, useEffect, useRef } from 'react'
import { MainContainer } from '#ui/containers'
import { useWebsocket, useStore } from '#hooks';
import { CircleButton } from '#ui/inputs';
import { Counter, Canvas, UserBar } from '#gamecomponents';
import ResultPopup from '../components/result/ResultPopup';
import { pages } from '../../pages';
import { Modal } from '#overlay';
import { useHistory } from 'react-router-dom';
import styled from 'styled-components';
import { Snackbar, Loader } from '#overlay';

export default function GuessingPage() {
    const mainRef = useRef(null);
    const [guess, setGuess] = useState('');
    const [player] = useStore('player');
    const [showSnackbar, setShowSnackbar] = useState(false);
    const [showLoader, setShowLoader] = useState(true);
    const [roundInfo, setRoundInfo] = useStore('roundInfo');
    const [players, setPlayers] = useStore('players');
    const [previousWord, setPreviousWord] = useStore('previousWord', undefined);
    const [showModal, setShowModal] = useState(false);
    const [creator, setCreator] = useState(undefined);
    const { sendGuess, onGuessOutcome, onRoundStarted, onRoundEnded, emitWebsocket, onPlayerUpdated } = useWebsocket();
    const [start, setStart] = useState(false);
    const [showTextBox, setShowTextBox] = useState(true);
    const history = useHistory();
    const [forced, setForced] = useStore('forced', false);

    const handleClick = () => {
        sendGuess({ guess: guess.toLowerCase(), player: player });
    }

    const handleChange = (event) => {
        const { value } = event.currentTarget;
        setGuess(value);
    }

    const handleEnter = (event) => {
        sendGuess({ guess: guess.toLowerCase(), player: player });
    }

    const onReadyToContinue = () => {
        if (roundInfo.creator === undefined) {
            history.push(pages.waitingRoom.path);
            return
        }

        // if we are the creator
        if (player.id === creator) {
            history.push(pages.creatorPage.path);
            return;
        }
        // On reste ici ?
        setShowModal(false);
        setShowLoader(true);
        setShowTextBox(false);
    }

    const timerEndedCallback = () => {
        setShowTextBox(false);
    }

    useEffect(() => {
        const guessOutcomeCallback = (payload) => {
            // payload = {player:playerId, correct:bool}
            if (payload.player === player.id) {
                setShowTextBox(!payload.correct);
                if (!payload.correct) {
                    setGuess('');
                }
                setShowSnackbar(true);
            }
            // handle incorrect guess
            emitWebsocket("update:getPlayers");
        }
        const roundStartedCallback = (payload) => {

            // payload
            setStart(true);
            setShowLoader(false);
            setShowSnackbar(false);
            setShowModal(false);
            setShowTextBox(true);
            setGuess('');
            // modal ?
            // 
        }
        const roundEndedCallback = (payload) => {
            setShowModal(true);
            const { creator, result, wordChoices, previousWord } = payload;
            setCreator(creator);
            setPlayers(result);
            setPreviousWord(previousWord)
            const roundInfo = {
                creator: creator,
                wordChoices: wordChoices
            }
            setRoundInfo(roundInfo);
            setStart(false);
        }

        // const playerUpdatedCallback = (payload) => {
        //     console.log(payload);
        // }

        if (forced) {
            setStart(true);
            setShowLoader(false);
            setShowSnackbar(false);
            setShowModal(false);
            setGuess('');
            setForced(false);
        }

        onGuessOutcome(guessOutcomeCallback);
        onRoundStarted(roundStartedCallback);
        onRoundEnded(roundEndedCallback);
        // onPlayerUpdated(playerUpdatedCallback);
    }, [forced])

    return (
        <MainContainer ref={mainRef}>
            <HeaderWrapper>
                <Counter start={start} callback={timerEndedCallback} />
                <UserBar />
            </HeaderWrapper>
            <Canvas />
            {
                showTextBox &&
                <TextBoxInput onEnter={handleEnter} onClick={handleClick} onChange={handleChange} value={guess} />
            }
            {showModal &&
                <Modal open={showModal} onClose={() => { setShowModal(false); }} noClose={true}>
                    <ResultPopup callback={onReadyToContinue} />
                </Modal>
            }

            {showSnackbar &&
                <Snackbar open={showSnackbar} onClose={() => setShowSnackbar(false)}>
                    <GuessOutcomeDisplay correct={showTextBox} />
                </Snackbar>
            }
            {showLoader &&
                <Loader parent={mainRef} open={showLoader}>
                    <LoaderMessage>
                        En attente du début de la ronde...
                    </LoaderMessage>
                </Loader>
            }
        </MainContainer>
    )
}

const LoaderMessage = styled.div`
    margin-top: 2rem;
    color: #fff;
    font-size: 1.4rem;
    max-width: 18rem;
    text-align: center;
`

const GuessOutcomeDisplay = ({ correct }) => {
    let message = "Ce n'est pas le mot desiré";
    if (!correct) {
        message = "Bravo! vous avez devinez correctement"
    }

    return (
        <div>
            {message}
        </div>
    )
}

const HeaderWrapper = styled.div`
    position: absolute;
    top: .6rem;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    gap: .5rem;
`

const TextInput = styled.input.attrs({
    type: 'text'
})`
    border: none;
    background-color:#2b62cc;
    border-radius: 1.5rem;
    width: 19rem;
    font-size:1.4rem;
    outline: none;
    padding: 0 1rem;
    color: #fff;
    box-shadow: 0 0 2px 2px rgba(0, 0, 0, 0.2);
    &:focus{
        box-shadow: 0 0 5px 5px rgba(0, 0, 0, 0.2);
    }
`

const TextBoxInputWrapper = styled.div`
    position: absolute;
    display: flex;
    bottom: 2rem;
    left: 50%;
    transform: translate(-50%, 0);
    gap: .6rem;
`

const TextBoxInput = ({ onChange, onEnter, onClick, value }) => {
    const handleKeydown = (event) => {
        if (event.key === 'Enter') {
            onEnter()
        }
    }

    return (
        <TextBoxInputWrapper>
            <TextInput onChange={onChange} onKeyDown={handleKeydown} value={value} />
            <CircleButton style={{ padding: '.3rem .1rem 0 0' }} onClick={onClick}>
                <svg width="40" height="40" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path id="Send_2" d="M15.8325 8.17463L10.109 13.9592L3.59944 9.88767C2.66675 9.30414 2.86077 7.88744 3.91572 7.57893L19.3712 3.05277C20.3373 2.76963 21.2326 3.67283 20.9456 4.642L16.3731 20.0868C16.0598 21.1432 14.6512 21.332 14.0732 20.3953L10.106 13.9602" stroke="#fff" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                </svg>
            </CircleButton>
        </TextBoxInputWrapper>
    )
}
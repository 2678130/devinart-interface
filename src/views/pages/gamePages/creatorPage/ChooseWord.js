import React, { useEffect } from 'react';
import { ButtonWordPick } from '#ui/inputs';
import { useStore, useWebsocket } from "#hooks";
import eventNames from '#eventnames';
import './ChooseWord.css';

export default function ChooseWord({ onClose, start }) {
    const [roundInfo] = useStore('roundInfo', {});
    const [player] = useStore('player');
    const words = roundInfo.wordChoices || ["123"];

    const { emitWebsocket, onRoundStarted } = useWebsocket();

    const handleClick = (event) => {
        const name = event.currentTarget.getAttribute('name');
        start();
        emitWebsocket(eventNames.wordChosen, { word: name, player: player })
    }

    useEffect(() => {
        const chosenWordCallback = (event) => {
            onClose();
        }
        onRoundStarted(chosenWordCallback);
    })

    return (
        <div className="choose-word">
            <div className="choose-word-to-draw">
                {words.map((word, index) => {
                    return <ButtonWordPick key={index} name={word} onClick={handleClick} />
                })}
            </div>
        </div>
    )
}
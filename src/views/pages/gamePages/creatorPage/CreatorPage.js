
import { MainContainer } from '#ui/containers';
import { Canvas } from "../components/canvas";
import { ButtonQuestionMarkBlue } from '#ui/inputs';
import React, { useEffect, useState } from 'react';
import { Modal } from '#overlay';
import ChooseWord from './ChooseWord';
import { Counter, DrawingMenu, UserBar } from '#gamecomponents';
import { useWebsocket, useStore } from '#hooks';
import ResultPopup from '../components/result/ResultPopup';
import { useHistory } from 'react-router-dom'
import { pages } from '../../pages';
import styled from 'styled-components';


export default function CreatorPage() {
    // useState pour couleur
    // useState pour largeur ligne
    const history = useHistory();
    const { onRoundEnded, onRoundStarted } = useWebsocket();
    const [openModal, setOpenModal] = useState(true);
    const [start, setStart] = useState(false);
    const [showChooseWord, setShowChooseWord] = useState(true);
    const [showResults, setShowResults] = useState(false);
    const [players, setPlayers] = useStore('players');
    const [roundInfo, setRoundInfo] = useStore('roundInfo');
    const [forced, setForced] = useStore('forced', false);
    const [canDraw, setCanDraw] = useState(false);
    const [player] = useStore('player')
    const [previousWord, setPreviousWord] = useStore('previousWord');

    const continueCallback = () => {
        // Game ended return to waiting room
        if (roundInfo.creator === undefined) {
            history.push(pages.waitingRoom.path);
            return
        }
        history.push(pages.guessingPage.path);
    }

    const startCallback = () => {
        setOpenModal(false);
        setShowChooseWord(false);
        setStart(true);
        setCanDraw(true);
    }

    const onTimerEndedCallback = () => {
        setCanDraw(false);
    }

    useEffect(() => {
        const roundStartedCallback = (payload) => {
            if (payload === player.id) {
                return;
            }
            setForced(true);
            history.push(pages.guessingPage.path);
        }

        const roundEndedCallback = (payload) => {
            // {
            // result: result
            // creators: creators
            // }
            const { creator, result, wordChoices, previousWord } = payload;
            setOpenModal(true);
            setShowResults(true);
            setPlayers(result);
            setPreviousWord(previousWord);
            const roundInfo = {
                creator: creator,
                wordChoices: wordChoices
            }
            setRoundInfo(roundInfo);
            setStart(false);
        }

        onRoundStarted(roundStartedCallback);
        onRoundEnded(roundEndedCallback);
    }, [])


    return (
        <MainContainer>
            <HeaderWrapper>
                <Counter start={start} callback={onTimerEndedCallback} />
                <UserBar />
            </HeaderWrapper>
            <Canvas draw={canDraw} />
            {openModal &&
                <Modal onClose={() => setOpenModal(false)} open={openModal} noClose={true}>
                    {showChooseWord && <ChooseWord onClose={() => { setOpenModal(false); }} start={startCallback} />}
                    {showResults && <ResultPopup callback={continueCallback} />}
                </Modal>
            }
            <DrawingMenu />
        </MainContainer>
    )
}


const HeaderWrapper = styled.div`
    position: absolute;
    top: .6rem;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    gap: .5rem;
`
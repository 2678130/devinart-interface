import React, { useEffect, useState } from 'react';
import { useStore } from '#hooks';
import { UserDisplay } from '#components/user';
import styled from 'styled-components';

const ResultPopup = ({ callback }) => {
    const [players] = useStore('players');
    const [roundInfo] = useStore('roundInfo');
    const [previousWord] = useStore('previousWord');

    const buttonMessage = roundInfo.creator === undefined ? 'Partie terminé' : 'Prochaine ronde'
    const titleMessage = roundInfo.creator === undefined ? 'POINTAGE FINAL' : 'POINTAGE'

    // {
    //   id,
    //   name,
    //   host,
    //   points
    // }
    // array players => [joueur1, joueur2, joueur3 ...]

    // players.sort((p1, p2) => p1.points - p2.points)

    // Next round

    return (
        <ResultPopupWrapper>
            <ResultPopupSpan>{titleMessage}</ResultPopupSpan>
            {
                previousWord !== undefined && 
                <ResultPopupSpan>MOT : {previousWord}</ResultPopupSpan>
            }

            {players.map((player, index) => {
                return <UserDisplay name={player.name} avatar={player.avatar} points={player.points} key={index} index={index} />
            }).sort((p1, p2) => p1.points - p2.points)}
            <ContinueButton onClick={callback}>{buttonMessage}</ContinueButton>
        </ResultPopupWrapper>
    );
}


const ResultPopupWrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    gap: .2rem;
    margin: .1rem;
`

const ResultPopupSpan = styled.span`
    font-size: 2rem;
    font-weight: lighter;
    font-style: italic;
    margin: .6rem;
`

const ContinueButton = styled.button`
    background-color: #2b62cc;
    font-family: 'Montserrat', sans-serif;
    font-size: 1.5rem;
    color: #fff;
    border: none;
    height: 3.5rem;
    width: 100%;
    border-radius: 999px;
    outline: none;
    margin: 1rem 0;
    box-shadow: 0 0 3px 3px rgba(0, 0, 0, 0.2);
    cursor: pointer;
`

export default ResultPopup;

import React from 'react';
import styled from 'styled-components';
import './UserAvatar.css';
import { Avatar } from '#components/user'
import { useStore } from '#hooks';

const UserAvatar = ({ avatar, player }) => {

    let style = { backgroundColor: "red" };
    const [roundInfo] = useStore('roundInfo');


    if (player.id === roundInfo.creator) {
        style.backgroundColor = "green";
    }


    return (
        <UserAvatarWrapper>
            {player.correct && <UserAvatarBadge />}
            {(player.id === roundInfo.creator) && <UserAvatarBadgeCreator />}
            <Avatar width={2} index={player.avatar}/>
            <Name>
                {player.name.substring(0, 3)}
            </Name>

        </UserAvatarWrapper>
    );
};

export default UserAvatar;

const Name = styled.div`
    position: absolute;
    color: #000;
    bottom: -.6rem;
    font-size: .4rem;
    font-weight: bold;
    padding: 0 .2rem;
    background-color: #fff;
    border-radius: 3px;

`

const UserAvatarWrapper = styled.div`
    position: relative;
    border-radius: 50%;
    background-color: transparent;
    width: 2rem;
    height: 2rem;
    color: #fff;
    display: flex;
    align-items: center;
    justify-content: center;
`


const UserAvatarBadge = () => {
    return (
        <div className="user-avatar-badge">
            <svg width="26" height="26" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M12 22C6.47967 21.9939 2.00606 17.5203 2 12V11.8C2.10993 6.30453 6.63459 1.92796 12.1307 2.00088C17.6268 2.07381 22.0337 6.56889 21.9978 12.0653C21.9619 17.5618 17.4966 21.9989 12 22ZM7.41 11.59L6 13L10 17L18 9L16.59 7.58L10 14.17L7.41 11.59Z" fill="#4dce33" />
            </svg>
        </div>
    );
}
const UserAvatarBadgeCreator = () => {
    return (
        <div className="user-avatar-badge" style={{transform:'rotate(-75deg) translate(0px, 2px)'}}>
            <svg width="26" height="26" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M4.41999 20.579C4.13948 20.5785 3.87206 20.4602 3.68299 20.253C3.49044 20.0475 3.39476 19.7695 3.41999 19.489L3.66499 16.795L14.983 5.481L18.52 9.017L7.20499 20.33L4.51099 20.575C4.47999 20.578 4.44899 20.579 4.41999 20.579ZM19.226 8.31L15.69 4.774L17.811 2.653C17.9986 2.46522 18.2531 2.35971 18.5185 2.35971C18.7839 2.35971 19.0384 2.46522 19.226 2.653L21.347 4.774C21.5348 4.96157 21.6403 5.21609 21.6403 5.4815C21.6403 5.74691 21.5348 6.00143 21.347 6.189L19.227 8.309L19.226 8.31Z" fill="#F3802C" />
            </svg>
        </div>
    );
}
import React, { useEffect, useState } from 'react';
import { useStore, useWebsocket } from '#hooks';
import UserAvatar from './UserAvatar';
import './UserBar.css'

const UserBar = () => {
    const [players, setPlayers] = useStore('players', []);
    const [correctPlayers, setCorrectPlayers] = useState(players);
    const { onPlayerUpdated, onGuessOutcome, onRoundEnded } = useWebsocket();

    useEffect(() => {
        const roundEndedCallback = () => {
            setCorrectPlayers([]);
            setCorrectPlayers(players.map(p => {
                p.correct = false;
                return p;
            }));
        }

        const playerUpdateCallback = (payload) => {
            setPlayers(payload);
            // correct players => players 1 propriete de plus p.correct => 
            // payload => tous les joueurs p.correct X
            // possible que payload [3] joueurs, correct players [4] pcq 1 
            // joueur qui a quitter
            // [p1, p2.correct, p4] [p1, p2.correct, p3, p4]
            // [p1.id, p2.id, p4.id]
            const ids = payload.map(p => p.id)

            // [p1, p2, p3, p4] => [p1, p2, undefined, p4].filter(() => )
            // [p1, p2, p4]

            const newCorrectPlayers = correctPlayers.map(p => {
                if (ids.includes(p.id)) {
                    return p
                }
                return undefined;
            }).filter(p => p !== undefined);

            setCorrectPlayers(newCorrectPlayers);
        }


        const wordGuessedCallback = (payload) => {
            const updatedPlayers = correctPlayers.map(p => {
                if (p.id === payload.player) {
                    p.correct = payload.correct;
                }
                return p;
            })
            setCorrectPlayers(updatedPlayers);
        }

        onPlayerUpdated(playerUpdateCallback);
        onGuessOutcome(wordGuessedCallback);
        onRoundEnded(roundEndedCallback);
    }, [])

    return (
        <div className="game-user-bar">
            {correctPlayers.map((player, index) => {
                return <UserAvatar player={player} key={index} />
            })}
        </div>
    );
};

export default UserBar;
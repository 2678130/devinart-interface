
import { useEffect, useRef } from 'react';
import { CircleButton } from '#ui/inputs';
import { animated, useSpring } from 'react-spring';
import Store from '#store';
import styled from 'styled-components';

const ColorChoice = ({ }) => {
    const ref = useRef(null);
    const [props, set] = useSpring(() => ({
        opacity: 0,
    }))

    const toggleSlider = () => {
        let opacity = 0

        if (props.opacity.value === 0) {
            opacity = 1;
            ref.current.style.display = 'flex';
        }

        set({
            opacity: opacity,
            onRest: hide
        })
    }

    const hideSlider = () =>{
        
    }

    const hide = () => {
        if (props.opacity.value === 0) {
            ref.current.style.display = 'none';
        }
    }

    // const hideChoices = () => {
    //     set({
    //         opacity: 1,
    //         height: 1000
    //     })
    // }

    useEffect(() => {
        // window.addEventListener('click', hideChoices);

        return () => {
            // window.removeEventListener('click', hideChoices);
        };
    }, []);

    const changeColor = (event) => {
        const colorCode = event.currentTarget.getAttribute("name");
        Store.setState("colorCode", colorCode);
        if (Store.getState("penSize") === 50) {
            Store.setState("penSize", 8);
        }
    }

    return (
        <ChoiceWrapper onClick={toggleSlider}>
            <svg width="36" height="36" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M4.41999 20.579C4.13948 20.5785 3.87206 20.4602 3.68299 20.253C3.49044 20.0475 3.39476 19.7695 3.41999 19.489L3.66499 16.795L14.983 5.481L18.52 9.017L7.20499 20.33L4.51099 20.575C4.47999 20.578 4.44899 20.579 4.41999 20.579ZM19.226 8.31L15.69 4.774L17.811 2.653C17.9986 2.46522 18.2531 2.35971 18.5185 2.35971C18.7839 2.35971 19.0384 2.46522 19.226 2.653L21.347 4.774C21.5348 4.96157 21.6403 5.21609 21.6403 5.4815C21.6403 5.74691 21.5348 6.00143 21.347 6.189L19.227 8.309L19.226 8.31Z" fill="#fff" />
            </svg>
            <SlideItemWrapper style={props} ref={ref}>
                <CircleButton name="#EA3737" onClick={changeColor}>
                    <PencilSize size={2} color='#EA3737' />
                </CircleButton>
                <CircleButton name="#FE9041" onClick={changeColor}>
                    <PencilSize size={2} color='#FE9041' />
                </CircleButton>
                <CircleButton name="#44CC14" onClick={changeColor}>
                    <PencilSize size={2} color='#44CC14' />
                </CircleButton>
                <CircleButton name="#143CCC" onClick={changeColor}>
                    <PencilSize size={2} color='#143CCC' />
                </CircleButton>
                <CircleButton name="#9C5D30" onClick={changeColor}>
                    <PencilSize size={2} color='#9C5D30' />
                </CircleButton>
                <CircleButton name="#858585" onClick={changeColor}>
                    <PencilSize size={2} color='#858585' />
                </CircleButton>
                <CircleButton name="#8614CC" onClick={changeColor}>
                    <PencilSize size={2} color='#8614CC' />
                </CircleButton>
                <CircleButton name="#F78BAA" onClick={changeColor}>
                    <PencilSize size={2} color='#F78BAA' />
                </CircleButton>
            </SlideItemWrapper>
        </ChoiceWrapper>
    );
}

const ChoiceWrapper = styled.div`
    position: relative;
    display: flex;
    align-items: center;
    justify-content: center;
    width: 3.5rem;
    height: 3.5rem;
    border-radius: 50%;
    border: none;
    padding:0;
    background-color: #2b62cc;
    box-shadow: 0 0 3px 3px rgba(0, 0, 0, 0.2);
    cursor: pointer;
    outline: none;
    &:focus{
        box-shadow: 0 0 8px 8px rgba(0, 0, 0, 0.4);
        outline: none;
    }
`

const SlideItemWrapper = styled(animated.div)`
    position: absolute;
    top:-.5rem;
    left:50%;
    transform: translate(-50%,-100%);
    display: flex;
    flex-direction: column;
    gap: .2rem;
`
const PencilSize = styled.div`
    width: ${props => props.size}rem;
    height: ${props => props.size}rem;
    border-radius:  50%;
    background-color: ${props => props.color};
`

export default ColorChoice;
import React, { useRef, useState, useEffect } from 'react';
import styled from 'styled-components';
import { CircleButton } from '#ui/inputs';
import { SlidingMenu } from '#ui/menu';
import Store from '#store';
import { useStore } from '#hooks';


const DrawingMenuWrapper = styled.div`
    position: absolute;
    bottom: 2rem;
    display: flex;
    flex-direction: row;
    gap: 0.5rem;
`

const DrawingMenu = ({ }) => {
    const [_, setReset] = useStore('resetCanvas');

    const actions = {
        reset: 'reset',
        size: 'size',
        erase: 'erase',
        colors: 'colors',
        default1: 'default1',
        default2: 'default2',
    }

    const size = {
        small: 3,
        medium: 8,
        large: 15,
        huge: 30
    }
    const displaySize = {
        small: .5,
        medium: 1,
        large: 1.5,
        huge: 2.5
    }
    const colors = [
        '#EA3737', '#FE9041', '#44CC14', '#143CCC', '#9C5D30', '#858585', '#8614CC', '#F78BAA'
    ]


    const changeToEraser = (event) => {
        const colorCode = event.currentTarget.getAttribute("name");
        Store.setState("colorCode", colorCode);
        Store.setState("penSize", 50);
    }

    const changeColor = (event) => {
        const colorCode = event.currentTarget.getAttribute("name");
        Store.setState("colorCode", colorCode);
        if (Store.getState("penSize") === 50) {
            Store.setState("penSize", 8);
        }
    }
    const clearCanvas = () => {
        setReset(true);
    }
    const changeSize = (event) => {
        const penSize = event.currentTarget.getAttribute("name");
        Store.setState("penSize", Number(penSize));
    }
    return (
        <DrawingMenuWrapper>
            <SlidingMenu callback={changeSize}>
                <CircleButton>
                    <svg width="36" height="36" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M4.41999 20.579C4.13948 20.5785 3.87206 20.4602 3.68299 20.253C3.49044 20.0475 3.39476 19.7695 3.41999 19.489L3.66499 16.795L14.983 5.481L18.52 9.017L7.20499 20.33L4.51099 20.575C4.47999 20.578 4.44899 20.579 4.41999 20.579ZM19.226 8.31L15.69 4.774L17.811 2.653C17.9986 2.46522 18.2531 2.35971 18.5185 2.35971C18.7839 2.35971 19.0384 2.46522 19.226 2.653L21.347 4.774C21.5348 4.96157 21.6403 5.21609 21.6403 5.4815C21.6403 5.74691 21.5348 6.00143 21.347 6.189L19.227 8.309L19.226 8.31Z" fill="#fff" />
                    </svg>
                </CircleButton>
                {Object.keys(size).map((key, index) => {
                    return (
                        <CircleButton name={size[key]} key={index}>
                            <ColorCircle size={displaySize[key]} color='#fff' />
                        </CircleButton>)
                })}
            </SlidingMenu>
            <CircleButton onClick={changeToEraser} name="#fff">
                <svg width="34" height="36" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 330 330" >
                    <path d="M315,285H201.214l124.393-124.394c5.858-5.857,5.858-15.355,0-21.213l-120-120 c-5.857-5.858-15.355-5.858-21.213,0l-180,179.999C1.58,202.205,0,206.02,0,209.999s1.58,7.794,4.394,10.607l90,90 c2.813,2.813,6.628,4.393,10.606,4.393L165,315c0.006,0,0.011-0.001,0.017-0.001L315,315c8.283,0,15-6.716,15-15 C330,291.716,323.284,285,315,285z M195,51.213L293.787,150L207,236.787L108.213,138L195,51.213z" fill="#fff" />
                </svg>
            </CircleButton>
            <CircleButtonWrapper>
                <CircleButton style={{ boxShadow: 'none' }} onClick={changeColor} name='#0a0a0a'>
                    <ColorCircle size={2.6} color='#0a0a0a' />
                </CircleButton>
                <CircleButton style={{ boxShadow: 'none' }} onClick={changeColor} name='yellow'>
                    <ColorCircle size={2.6} color='yellow' />
                </CircleButton>
                <SlidingMenu callback={changeColor}>
                    <CircleButton style={{ boxShadow: 'none' }}>
                        <svg width="44" height="44" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12 22C6.47967 21.9939 2.00606 17.5203 2 12V11.8C2.10993 6.30452 6.63459 1.92794 12.1307 2.00087C17.6268 2.07379 22.0337 6.56887 21.9978 12.0653C21.9619 17.5618 17.4966 21.9989 12 22ZM7 11V13H11V17H13V13H17V11H13V6.99999H11V11H7Z" fill="#fff" />
                        </svg>
                    </CircleButton>
                    {colors.map((color, index) => {
                        return (
                            <CircleButton name={color} key={index}>
                                <ColorCircle size={2} color={color} />
                            </CircleButton>)
                    })}
                </SlidingMenu>
            </CircleButtonWrapper>
            <CircleButton onClick={clearCanvas} name={actions.reset}>
                <svg width="34" height="34" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M17 22H7C5.89543 22 5 21.1046 5 20V7H3V5H7V4C7 2.89543 7.89543 2 9 2H15C16.1046 2 17 2.89543 17 4V5H21V7H19V20C19 21.1046 18.1046 22 17 22ZM7 7V20H17V7H7ZM9 4V5H15V4H9Z" fill="#fff" />
                </svg>
            </CircleButton>

        </DrawingMenuWrapper>
    );
}

export default DrawingMenu;

const CircleButtonWrapper = styled.div`
    display: flex;
    flex-direction: row;
    background-color: #2b62cc;
    border-radius: 999px;
    box-shadow: 0 0 3px 3px rgba(0, 0, 0, 0.2); 
`

const ColorCircle = styled.div`
    width: ${props => props.size}rem;
    height: ${props => props.size}rem;
    border-radius:  50%;
    background-color: ${props => props.color};
    pointer-events: none;
`

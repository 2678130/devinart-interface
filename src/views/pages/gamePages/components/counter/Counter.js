import React, { useRef } from 'react';
import "./Counter.css";
import { useState, useEffect } from 'react';

export default function Counter({ start, callback }) {
    // du code ici, timer start,
    const [time, setTime] = useState(59);

    useEffect(() => {
        if (time > 0 && start) {
            setTimeout(() => setTime(time - 1), 1000);
        }
        if (time === 0 && start) {
            callback();
        }
        if (time === 0 && !start) {
            setTime(59);
        }

    }, [time, start])



    return (
        <div className="circle-counter">
            <div className="counter-time">{time}</div>
        </div>
    )
}


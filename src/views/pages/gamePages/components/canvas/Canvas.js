import React, { useRef, useEffect } from 'react';
import { useGesture } from 'react-use-gesture';
import { useWebsocket } from '#hooks'
import "./Canvas.css";
import Store from '#store';
import { useStore } from '#hooks';

export const Canvas = ({ draw }) => {
    const canvasRef = useRef(null);
    const context = useRef(null);
    const previousXY = useRef(null);
    const drawing = useRef(null);
    const { onDraw, onDrawingUpdated } = useWebsocket();
    const [reset, setReset] = useStore('resetCanvas', false);
    let colorCode = Store.getState("colorCode");
    let penSize = Store.getState("penSize");


    // appel le serveur

    // function to bind as listener when drawing
    // bind retourne 3 fonctions
    // addEventListener => mousemove // mousedown // mouseup //
    let bind = useGesture({
        onDrag: ({ xy }) => {
            xy = correctXy(xy);
            drawPoint({ xy: xy, color: colorCode, lineWidth: penSize });
            drawing.current.push({ xy: xy, color: colorCode, lineWidth: penSize });

            onDraw(drawing.current);

        },
        onDragStart: ({ xy }) => {
            colorCode = Store.getState("colorCode");
            penSize = Store.getState("penSize");
            xy = correctXy(xy);
            previousXY.current = xy;
        },
        onDragEnd: () => {
            // line ended
            drawing.current.push({ xy: undefined });
        }
    });


    // if not drawing set bind to null
    bind = draw ? bind : () => null;

    // helper function to account for mouse/finger position of varying devices
    const correctXy = (xy) => {
        const boundaries = canvasRef.current.getBoundingClientRect();
        const xOffset = boundaries.x;
        const yOffset = boundaries.y;
        return xy.map((value, index) => index === 0 ? value - xOffset : value - yOffset)
    }

    /**
     * Drawing to canvas function
     * @param {point object} param0 
     */
    const drawPoint = ({ xy, lineWidth, color }) => {
        const ctx = context.current;

        // stuff to account for a new line start from  
        if (!previousXY.current) {
            previousXY.current = xy;
            return;
        }
        const previousPoint = previousXY.current;
        if (xy === undefined) {
            previousXY.current = undefined;
            return;
        }

        // drawing stuff
        const previousX = previousPoint[0];
        const previousY = previousPoint[1];
        ctx.beginPath();
        ctx.lineCap = 'round'; //square
        ctx.lineWidth = lineWidth || 8;
        ctx.strokeStyle = color || '#0a0a0a';
        ctx.moveTo(previousX, previousY);
        ctx.lineTo(xy[0], xy[1]);
        ctx.stroke();

        // previous point is next poing
        previousXY.current = xy;
    }


    //  array item = {
    //      xy: [0,0],
    //      lineWWidth,
    //      color
    //  }
    /**
     * Reconciliation function
     * 
     * [{}, {}]
     * 
     * @param {array of point object} pointsArray 
     */
    const updateDrawingCallback = (pointsArray) => {

        if (drawing.current.length > pointsArray.length) {
            context.current.clearRect(0, 0, canvasRef.current.width, canvasRef.current.height);
            drawing.current = [];
            return;
        }

        // drawing.current => questce qui a ete dessiner [abc def]
        // TODO envoyer uniquement linformation necessaire
        previousXY.current = drawing.current.length > 0 ? drawing.current.pop().xy : pointsArray[0].xy;
        let start = drawing.current.length;
        const pointsToDraw = pointsArray.slice(start, pointsArray.length);

        pointsToDraw.forEach(point => {
            drawPoint(point);
        });

        drawing.current = pointsArray;
    }

    useEffect(() => {
        // init draw
        const boundaries = canvasRef.current.getBoundingClientRect();
        context.current = canvasRef.current.getContext('2d');
        context.current.canvas.width = boundaries.width;
        context.current.canvas.height = boundaries.height;
        drawing.current = [];

        onDrawingUpdated(updateDrawingCallback)

        if (reset) {
            context.current.clearRect(0, 0, canvasRef.current.width, canvasRef.current.height);
            drawing.current = [];
            onDraw(drawing.current);
            setReset(false);
        }

        // if (forced) {
        //     context.current.clearRect(0, 0, canvasRef.current.width, canvasRef.current.height);
        // }

        return () => {
        };
    }, [reset]);

    return (
        <div className="default-canvas-container">
            <canvas {...bind()} className="default-canvas" ref={canvasRef} >
            </canvas>
        </div>
    )
}

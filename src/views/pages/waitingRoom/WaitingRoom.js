import React, { useEffect } from 'react';
// import css
import "./WaitingRoom.css";
import { MainContainer } from '#ui/containers';
import { ButtonOrange } from "#ui/inputs";
import { useStore, useWebsocket } from '#hooks';
import { useHistory } from 'react-router-dom';
import { pages } from '../pages';
import eventNames from '#eventnames'
import UserDisplay from '../../components/user/UserDisplay';

export default function WaitingRoom() {
    const [players, setPlayers] = useStore('players', []);
    const [player, setPlayer] = useStore('player');
    const [roundInfo, setRoundInfo] = useStore('roundInfo');
    const { emitWebsocket, onGameStarted, onPlayerUpdated } = useWebsocket();
    const history = useHistory();

    const action = {
        start: "start"
    }
    const handleClick = (event) => {
        const { name } = event.target;
        if (name === action.start) {
            emitWebsocket(eventNames.startGame, player);
        }
    }


    useEffect(() => {
        const gameStartedCallback = (payload) => {

            setRoundInfo(payload);
            // on create
            if (payload.creator === player.id) {
                history.push(pages.creatorPage.path);
                return;
            }
            // on guess
            history.push(pages.guessingPage.path);
        }
        const playerUpdatedCallback = (payload) => {
            payload.forEach(p => {
                if(p.id === player.id){
                    player.host = p.host;
                    setPlayer(player);
                }
            })
            setPlayers(payload);
        }
        onGameStarted(gameStartedCallback);
        onPlayerUpdated(playerUpdatedCallback);
        emitWebsocket(eventNames.getPlayers);
    }, [player])

    
    return (
        <MainContainer style={{ marginBottom: '6rem' }}>
            <div className="waiting-room-container">
                <div className="players-list">
                
                    {players.map((player, index) => {
                        return <UserDisplay
                            name={player.name}
                            id={player.id}
                            avatar={player.avatar}
                            index={index}
                            key={index} />;
                    })}
                </div>
            </div>
            {player.host &&
                <div className="start-button">
                    <ButtonOrange onClick={handleClick} name={action.start}>Démarrer la partie</ButtonOrange>
                </div>
            }
        </MainContainer>
    )
}

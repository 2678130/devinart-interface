import React from 'react';
import Counter from '../gamePages/components/counter/Counter';
import UserDisplay from '../../components/user/UserDisplay';

const PageEssais = () => {
    return (
        <div>
            <Counter/>
            <UserDisplay name="BenWathier"  master={false} index={0} points={123} />
            <UserDisplay name="BenWathier"  master={false} index={1} points={321} />
            <UserDisplay name="BenWathier"  master={false} index={2} points={678} />
            <UserDisplay name="BenWathier"  master={false} index={3} points={1} />
            <UserDisplay name="BenWathier"  master={false} index={4} points={0} />
        </div>
    );
}

export default PageEssais;

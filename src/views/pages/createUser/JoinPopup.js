import React, { useState, useEffect } from 'react';
import "./JoinPopup.css";
import { TextBox } from "../../../components/inputs/TextBox";
import locksafe from "../../../imgs/lock_safe.png";
import { Button } from "../../../components/inputs/Button";
import { useWebsocket, useStore } from "#hooks";
import { useHistory } from "react-router-dom";
import { pages } from "../pages";
import eventNames from '#eventnames';
import { animated, useSpring } from 'react-spring';
import Snackbar from '../../../overlay/snackbar/Snackbar';
import styled from 'styled-components';

export default function JoinPopup({ openLoader, closeLoader }) {
    const [accessCode, setAccessCode] = useState();
    const { onGameJoined, emitWebsocket } = useWebsocket();
    const [_, setGameId] = useStore('accessCode');
    const [player] = useStore('player');
    const history = useHistory();
    const [open, setOpen] = useState(false);

    const handleEnter = (event) => {
        if (event.key === 'Enter') {
            websocketEmitJoinGame();
        }
    }
    const [props, set] = useSpring(() => ({
        transform: 'translate(0px, 0)',
    }))

    const handleClick = (event) => {
        websocketEmitJoinGame();
    }
    const handleChange = (event) => {
        const { value } = event.target;
        event.target.value = value.toUpperCase();
        setAccessCode(value.toUpperCase());
        setGameId(value.toUpperCase());
    }
    const websocketEmitJoinGame = () => {
        openLoader();
        emitWebsocket(eventNames.joinGame, { accessCode: accessCode, player: player });
    }


    useEffect(() => {
        const joinGameCallback = (payload) => {
            if (payload.success) {
                history.push(pages.waitingRoom.path);
                return
            }

            // code ici se fait executer
            closeLoader();
            setOpen(true);

            set({
                transform: 'translate(10px, 0)',
                config: {
                    tension: 455,
                    friction: 6
                },
            })
            setTimeout(() => {
                set({
                    transform: 'translate(0px, 0)',
                    config: {
                        tension: 455,
                        friction: 6
                    },
                })
            }, 80)
        }
        onGameJoined(joinGameCallback);
    }, [open])

    return (
        <animated.div className="join-popup" style={props}>
            <div className="join-popup-img">
                <img src={locksafe} alt="locksafe" />
            </div>
            <TextBox onChange={handleChange} onEnter={handleEnter} placeholder="Code d'accès" />
            <div className="join-popup-joindre">
                <Button onClick={handleClick} color="vert">Joindre la partie</Button>
            </div>
            {open &&
                <Snackbar open={open} onClose={() => { setOpen(false) }} height='-4rem'>
                    <JoiningGameMessage>
                        Code d'accès invalid
                    </JoiningGameMessage>
                </Snackbar>
            }
        </animated.div>
    )
}


const JoiningGameMessage = styled.div`
    color: #fff;
`
import React, { useEffect, useState, useRef } from 'react';
import { animated, useSpring } from 'react-spring';
import Modal from "../../../overlay/modal/Modal";
import { MainContainer } from '#ui/containers';
import { useHistory } from 'react-router-dom';
import './CreateUser.css';
import { useWebsocket, useStore } from '../../../hooks';
import JoinPopup from './JoinPopup';
import ChooseAvatar from './ChooseAvatar';
import { TextBox, Button } from "#ui/inputs";
import { pages } from '../pages';
import eventNames from '#eventnames'
import Snackbar from '../../../overlay/snackbar/Snackbar';
import styled from 'styled-components';
import Avatar from '../../components/user/Avatar';
import Loader from '../../../overlay/loader/Loader';

export default function CreateUser() {
    const action = {
        createGame: "createGame",
        join: "join",
        chooseAvatar: "chooseAvatar",
        next: 'next',
        previous: 'previous'
    };
    const mainContainerRef = useRef(null);
    const [player, setPlayer] = useStore('player');
    const [accessCode, setAccessCode] = useStore('accessCode');
    const [popup, setPopup] = useState(action.create);
    const [openModal, setOpenModal] = useState(false);
    const [openLoader, setOpenLoader] = useState(false);
    const [openSnackbar, setOpenSnackbar] = useState(false);
    const { emitWebsocket, onGameCreated, connectWebsocket } = useWebsocket();
    const history = useHistory();
    const [userProps, setUserProps] = useSpring(() => ({
        opacity: 1,
        transform: 'translate(-50%, -50%)'
    }))
    const [gameProps, setGameProps] = useSpring(() => ({
        opacity: 0,
        transform: 'translate(0%, -50%)',
        pointerEvents: 'none'
    }))
    const [containerProps, setContainerProps] = useSpring(() => ({
        transform: 'translate(0px, 0)',
    }))

    const next = (init) => {
        if (player.name.length < 3 && init) {
            return;
        }
        if (player.name.length < 3) {
            setOpenSnackbar(true);
            setContainerProps({
                transform: 'translate(10px, 0)',
                config: {
                    tension: 455,
                    friction: 6
                },
            })
            setTimeout(() => {
                setContainerProps({
                    transform: 'translate(0px, 0)',
                    config: {
                        tension: 455,
                        friction: 6
                    },
                })
            }, 80)
            return
        }
        connectWebsocket();
        setGameProps({
            opacity: 1,
            transform: 'translate(-50%, -50%)',
            pointerEvents: 'auto'
        })
        setUserProps({
            opacity: 0,
            transform: 'translate(-100%, -50%)',
            pointerEvents: 'none'
        })
        setPlayer({...player})
    }

    const handleClick = (event) => {
        const { name } = event.target;
        if (name === action.chooseAvatar) {
            setOpenModal(true);
        }
        if (name === action.createGame) {
            setOpenLoader(true);
            setPlayer(player);
            emitWebsocket(eventNames.createGame, player);
        }
        if (name === action.join) {
            setOpenModal(true);
        }
        if (name === action.next) {
            setOpenLoader(true);
            createUser(player);
        }
        if (name === action.previous) {
            setGameProps({
                opacity: 0,
                transform: 'translate(0%, -50%)',
                pointerEvents: 'none'
            })
            setUserProps({
                opacity: 1,
                transform: 'translate(-50%, -50%)',
                pointerEvents: 'auto'
            })
        }
        setPopup(action[name]);
    }

    const handleChange = (event) => {
        const { value } = event.currentTarget;
        setPlayer({ ...player, name: value })
    }

    const handleEnter = (event) => {
        if (event.key === 'Enter') {
        }
    }

    const createUser = async (data) => {


        // const response = await fetch('http://localhost:3003/user', {

        const response = await fetch('/user', {
            method: 'POST',
            mode: 'cors',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
        const payload = await response.json();

        player.id = payload.id;
        player.name = payload.name;
        player.avatar = payload.avatar;
        setPlayer(player);
        next(data === undefined);
        setOpenLoader(false);
    }

    useEffect(() => {
        createUser();
        const accessCodeCallback = (accessCode) => {
            setAccessCode(accessCode);
            history.push(pages.waitingRoom.path)
        }
        onGameCreated(accessCodeCallback);
    }, []);





    // Mobile
    return (
        <MainContainer ref={mainContainerRef} style={containerProps} >
            <animated.div className="default-create-user-inputs" style={userProps}>
                <Avatar index={player.avatar} onClick={handleClick} />
                <TextBox onChange={handleChange} placeholder="Nom d'utilisateur" onEnter={handleEnter} />
                <Button onClick={handleClick} name={action.chooseAvatar} color="orange">Choisir un avatar</Button>
                <Button onClick={handleClick} name={action.next} color="vert">Suivant</Button>
            </animated.div>

            <animated.div className="default-create-user-inputs" style={gameProps}>
                <Avatar index={player.avatar} />
                <PlayerNameWrapper>{player.name}</PlayerNameWrapper>
                <Button onClick={handleClick} name={action.createGame} color="orange">Créer une partie</Button> <Button onClick={handleClick} name={action.join} color="bleu">Rejoindre une partie</Button>
                <Button onClick={handleClick} name={action.previous} color="vert">Précédent</Button>
            </animated.div>

            { openModal &&
                <Modal open={openModal} onClose={() => setOpenModal(false)} parent={mainContainerRef}>
                    {popup === action.join && <JoinPopup openLoader={() => setOpenLoader(true)} closeLoader={() => setOpenLoader(false)} />}
                    {popup === action.chooseAvatar && <ChooseAvatar onClose={() => { setOpenModal(false) }} />}
                </Modal>
            }

            {openSnackbar &&
                <Snackbar open={openSnackbar} onClose={() => { setOpenSnackbar(false); }} >
                    <InvalidNameWrapper>
                        Entrez un nom d'utilisateur valide
                    </InvalidNameWrapper>
                </Snackbar>
            }
            {openLoader &&
                <Loader parent={mainContainerRef} open={openLoader}/>
            }
        </MainContainer>
    )
}



const PlayerNameWrapper = styled.div`
    font-size: 2.2rem;
    color: #fff;
    background-color: rgba(0, 0, 0, 0.6);
    border-radius: 999px;
    padding: .2rem 0;
`

const InvalidNameWrapper = styled.div`
    color: #fff;
`
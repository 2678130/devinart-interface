import React, { useEffect } from 'react';
import Avatar from '../../components/user/Avatar';
import './ChooseAvatar.css';
import { useStore } from '#hooks';


export default function ChooseAvatar({ onClose }) {
    const arr = new Array(6).fill(0);
    const [player, setPlayer] = useStore('player');



    const handleClick = (event) => {
        const name = event.currentTarget.getAttribute('name');
        player.avatar = Number(name);
        setPlayer(player);
        onClose();
    }

    return (
        <div className="popup-choose-avatar">
            <h1>Choisir votre avatar</h1>
            <div className="popup-choose-avatar-avatars">
                {arr.map((_, index) => {
                    const selected = index === player.avatar;
                    return <Avatar index={index} onClick={handleClick} selected={selected} key={index} />;
                })}
            </div>
        </div>
    )
}
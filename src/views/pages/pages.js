export const pages = {
    landing: {
        path:"/"
    },
    createUser:{
        path:"/Utilisateur"
    },
    waitingRoom:{
        path:"/SalleAttente"
    },
    creatorPage:{
        path:"/Createur"
    },
    guessingPage:{
        path:"/Devineur"
    },
    websocket:{
        // 127.0.0.1 => localhost
        path:"ws://localhost:3003"
    },
    essaisLp:{
        path:"/LP"
    }
}
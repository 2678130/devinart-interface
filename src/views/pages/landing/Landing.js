import React from 'react';
import { useHistory } from 'react-router-dom';
import { MainContainer } from '#ui/containers';
import "./Landing.css";
import { pages } from "#pages/pages";
import img_logo from '#imgs/devinart-logo/DevinARTS_logo.png';
import img_puppy from '#imgs/puppy.png';
import { ButtonQuestionMark } from "#ui/inputs";
import { useDetectMobile, useFullscreenApi } from '#hooks';

export default function Landing() {
    const history = useHistory();
    const fullscreeApi = useFullscreenApi();
    const isMobile = useDetectMobile();

    const handleRedirect = () => {
        history.push(pages.createUser.path);
        /*
        if(isMobile) {
            fullscreeApi.toggleFullscreen();
        }*/
    }
    const func = (event) => {
    }
    return (
        <MainContainer style={{ touchAction: 'none' }}>
            <div className="default-landing-question-mark">
                <ButtonQuestionMark onClick={func} />
            </div>
            <div className="default-landing-img-wrapper" onClick={handleRedirect} >
                <img src={img_logo} alt="DevinARTS" />
            </div>
            <div className="default-landing-puppy-img">
                <img src={img_puppy} alt="Puppy with toys" />
            </div>

        </MainContainer>
    )
}

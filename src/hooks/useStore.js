/**
 * State management wrapper
 */
import Store from "../state/store";
import { useEffect, useState, useRef} from "react";


export const useStore = (key, value) => {
    // CODE starts here

    // if value passed as argument, set the store state to the initial value
    if(value){
        Store.setInitialState(key, value);
    }

    const [state, setState] = useState(Store.getState(key));

    const updateValue = () => {
        setState(Store.getState(key));
    }

    const newValue = (value) => {
        Store.setState(key, value);
    }


    // We need to register for updates
    useEffect(() => {
        Store.subscribe(updateValue, key);
        return () => {
            // Cleanup
            Store.unsubscribe(updateValue, key);
        }
    }, [])

    return [
        state,
        newValue
    ]
}
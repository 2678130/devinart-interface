import React, { useRef, useState, useEffect } from 'react';

export const useFullscreenApi = (elementRef) => {
    const fullscreen = document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement
    const [isFullscreen, setIsFullscreen] = useState(fullscreen !== null);
    const ref = useRef(elementRef);
    const toggleFullscreen = async () => {
        try {
            if (isFullscreen) {
                exitFullscreen();
                return
            }
            launchFullscreen(ref.current);
            setIsFullscreen(true);
        }
        catch (e) {
            console.log(e);
            setIsFullscreen(false);
        }
    }
    useEffect(() => {
        const handleFullscreenChange = () => {
            const fullscreen = document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement
            setIsFullscreen(fullscreen !== null);
        }
        document.addEventListener('fullscreenchange', handleFullscreenChange);
        return () => {
            document.removeEventListener('fullscreenchange', handleFullscreenChange);
        };
    }, []);
    return { isFullscreen, toggleFullscreen }
}


function launchFullscreen(element) {
    element = element || document.body
    if (element.requestFullscreen) {
        element.requestFullscreen();
    } else if (element.mozRequestFullScreen) {
        element.mozRequestFullScreen();
    } else if (element.webkitRequestFullscreen) {
        element.webkitRequestFullscreen();
    } else if (element.msRequestFullscreen) {
        element.msRequestFullscreen();
    }
}

function exitFullscreen() {
    if (document.fullscreenElement === null) {
        throw new Error("Fullscreen is not enabled");
    }
    if (document.exitFullscreen) {
        document.exitFullscreen();
    } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
    } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
    }
}
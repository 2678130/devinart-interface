import { useEffect, useRef } from 'react';
import WebsocketController from '../state/WebsocketController';
import eventNames from '#eventnames';

export const useWebsocket = () => {
    // connection    
    const socketController = WebsocketController;
    // throttling
    const registeredFunctions = useRef(new Set());
    const THROTTLE_TIMING = 30;
    const lastupdate = useRef(0);

    const onDraw = (arrayOfPoints) => {
        const currentMillis = Date.now();
        // throttle the events
        if (currentMillis > (lastupdate.current + THROTTLE_TIMING)) {
            socketController.emitWebsocket(eventNames.onDraw, arrayOfPoints);
            lastupdate.current = currentMillis;
            return;
        }
    }
    const connectWebsocket = () => {
        socketController.connect();
    }

    const emitWebsocket = (eventName, data) => {
        socketController.emitWebsocket(eventName, data);
    }
    const onGameJoined = (callback) => {
        registeredFunctions.current.add(callback);
        socketController.registerGameJoined(callback);
    }
    const onGameStarted = (callback) => {
        registeredFunctions.current.add(callback);
        socketController.registerGameStarted(callback);
    }
    const onGameCreated = (callback) => {
        registeredFunctions.current.add(callback);
        socketController.registerGameCreated(callback);
    }
    const onGuessOutcome = (callback) => {
        registeredFunctions.current.add(callback);
        socketController.registerGuessOutcome(callback);
    }
    const onDrawingUpdated = (callback) => {
        registeredFunctions.current.add(callback);
        socketController.registerDrawingUpdated(callback);
    }
    const onWordChosen = (callback) => {
        registeredFunctions.current.add(callback);
        socketController.registerWordChosen(callback);
    }
    const onPlayerUpdated = (callback) => {
        registeredFunctions.current.add(callback);
        socketController.registerPlayerUpdated(callback);
    }
    const onRoundStarted = (callback) => {
        registeredFunctions.current.add(callback);
        socketController.registerRoundStarted(callback);
    }
    const onRoundEnded = (callback) => {
        registeredFunctions.current.add(callback);
        socketController.registerRoundEnded(callback);
    }
    

    const sendGuess = (guess) => {
        socketController.emitWebsocket(eventNames.guessWord, guess);
    }
    const getPlayers = () =>{
        socketController.emitWebsocket(eventNames.getPlayers);
    }


    // register websocket

    useEffect(() => {
        if (!registeredFunctions.current) {
            registeredFunctions.current = new Set();
        }

        return () => {
            registeredFunctions.current.forEach(fn => {
                socketController.unregisterFunction(fn);
            })
            registeredFunctions.current = undefined;
        }
    }, []);

    return {
        connectWebsocket,
        emitWebsocket,
        onDraw,
        onGameJoined,
        onGameStarted,
        onGameCreated,
        onGuessOutcome,
        onDrawingUpdated,
        onWordChosen,
        onPlayerUpdated,
        onRoundStarted,
        sendGuess,
        onRoundEnded,
        getPlayers
    }
}

import React, { useEffect, useRef } from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Header from './views/components/header/Header';
import Footer from './views/components/footer/Footer';
import Landing from './views/pages/landing/Landing';
import CreateUser from './views/pages/createUser/CreateUser';
import WaitingRoom from './views/pages/waitingRoom/WaitingRoom';
import { pages } from "./views/pages/pages";
import { Background } from './views/components/background/Background';
import CreatorPage from './views/pages/gamePages/creatorPage/CreatorPage';
import GuessingPage from './views/pages/gamePages/guessingPage/GuessingPage';
import PageEssais from './views/pages/page_essais_lp/PageEssais';
import Connection from './views/components/connection/Connection';
import Store from '#store';

function App() {
    Store.setInitialState('player', { name: '', avatar: 0, host: false })
    // JSX => traduit en code javascript
    return (
        <div className="App">
            <Router>
                <Connection />
                <Background />
                <Header />
                <Switch>
                    <Route path={pages.createUser.path}>
                        <CreateUser />
                    </Route>
                    <Route path={pages.waitingRoom.path}>
                        <WaitingRoom />
                    </Route>
                    <Route path={pages.guessingPage.path}>
                        <GuessingPage />
                    </Route>
                    <Route path={pages.creatorPage.path}>
                        <CreatorPage />
                    </Route>
                    <Route path={pages.essaisLp.path}>
                        <PageEssais />
                    </Route>
                    <Route path={pages.landing.path}>
                        <Landing />
                    </Route>
                </Switch>
                <Footer />
            </Router>
        </div>
    );
}

export default App;

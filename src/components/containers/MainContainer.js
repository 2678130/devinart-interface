import React from 'react';
import {animated} from 'react-spring'
import "./MainContainer.css";



const MainContainer = React.forwardRef(({style, className, children}, ref) => {
    className = className || ''; 



    return (
        <animated.main ref={ref} style={style} className={"default-main " + className} style={style}>
            {children}
        </animated.main>
    )
})


export {
    MainContainer
}
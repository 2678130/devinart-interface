import { Children, cloneElement, useEffect, useLayoutEffect, useRef } from 'react';
import { animated, useSpring } from 'react-spring';
import styled from 'styled-components';

const SlidingMenu = ({ callback, children }) => {
    if (!children instanceof Array) {
        throw new Error('Invalid Composition in SlidingMenu see documentation')
    }
    let menuButton = null;
    let menuItems = [];

    Children.forEach(children, (child, index) => {
        if (index === 0) {
            menuButton = child;
            return;
        }
        menuItems.push(child);
    });
    menuItems = menuItems.flat();
    const ref = useRef(null);
    const [props, set] = useSpring(() => ({
        opacity: 0,
        transform: 'translate(-50%, -50%)'
    }))

    const toggleSlider = () => {
        let opacity = 0
        let transform = 'translate(-50%, -50%)'
        if (props.opacity.value === 0) {
            opacity = 1;
            transform = 'translate(-50%, -100%)'
            ref.current.style.display = 'flex';
        }
        set({
            opacity: opacity,
            onRest: hide,
            transform: transform
        })
    }
    const hideSlider = (event) => {
        if (props.opacity.getValue() === 1) {
            set({
                opacity: 0,
                transform: 'translate(-50%, -50%)',
                onRest: hide
            })
        }
    }
    const hide = () => {
        if (props.opacity.getValue() === 0) {
            ref.current.style.display = 'none';
        }
    }
    const handleClick = (event) => {
        callback(event);
    }
    useEffect(() => {
        window.addEventListener('click', hideSlider);
        return () => {
            window.removeEventListener('click', hideSlider);
        };
    }, []);

    useLayoutEffect(() => {
        if (ref.current) {
            ref.current.style.display = 'none';
        }
    }, [])
    return (
        <ChoiceWrapper onClick={toggleSlider} >
            {menuButton && cloneElement(menuButton, { onClick: toggleSlider })}
            <SlideItemWrapper style={props} ref={ref}>
                {menuItems.map((child, index) => {
                    return cloneElement(child, { onClick: handleClick });
                })}
            </SlideItemWrapper>
        </ChoiceWrapper>
    );
}

const ChoiceWrapper = styled.div`
    position: relative;
    border: none;
    padding:0;
`

const SlideItemWrapper = styled(animated.div)`
    position: absolute;
    top:-.5rem;
    left:50%;
    transform: translate(-50%,-100%);
    display: flex;
    flex-direction: column;
    gap: .2rem;
`

export default SlidingMenu;
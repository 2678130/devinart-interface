import React from 'react';
import styled from 'styled-components';

const ButtonWrapper = styled.button`
    display: flex;
    align-items: center;
    justify-content: center;
    background-color: transparent;
    border: none;
    outline: none;
    width: 2.5rem;
    height: 2.5rem;
    border-radius: 50%;
    padding-right: .5rem;
    cursor: pointer;
    transition: all 0.3s ease;
    :hover& {
        transform: scale(1.2);
        transition: all 0.3s ease;
    }
    :active& {
        background-color: rgba(0, 0, 0, 0.2);
        transform: scale(1.2);
        transition: all 0.3s ease;
    }
`

const BackButton = ({ onClick, name }) => {
    return (
        <ButtonWrapper onClick={onClick} name={name}>
            <svg width="52" height="52" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" rx="2">
                <path d="M15.535 3.515L7.04999 12L15.535 20.485L16.95 19.071L9.87799 12L16.95 4.929L15.535 3.515Z" fill="#0a0a0a" />
            </svg>
        </ButtonWrapper>
    );
}

export default BackButton;

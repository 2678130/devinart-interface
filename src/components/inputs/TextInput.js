import React, { useState } from 'react';
import "./TextInput.css";

export const TextInput = ({ onChange, onBlur, onEnter, validation, name, placeholder, className }) => {

    className = className ? className : "default-text-input";
    placeholder = placeholder ? placeholder : "";
    name = name ? name : "";

    const handleChange = (event) => {
        onChange(event);
    }
    const handleBlur = (event) => {
        const { value, name } = event.currentTarget;
        if (validation) {
            validation(value);
        }
        if (onBlur) {
            onBlur(event);
        }
    }
    const handleEnter = (event) => {
        onEnter(event)
    }

    return (
        <>
            <input className={className} onKeyDown={handleEnter} placeholder={placeholder} type="text" onChange={handleChange} onBlur={handleBlur} />
        </>
    )
}

import React, {forwardRef} from 'react';
import styled from 'styled-components';

const CircleButton = forwardRef(({ children, style, color, onClick, name }, ref) => {
    color = color || '#2b62cc';
    name = name || '';

    return (
        <CircleButtonWrapper ref={ref} style={style} color={color} onClick={onClick} name={name}>
            {children}
        </CircleButtonWrapper>
    );
})

const CircleButtonWrapper = styled.button`
    display: flex;
    align-items: center;
    justify-content: center;
    width: 3.5rem;
    height: 3.5rem;
    border-radius: 50%;
    border: none;
    padding:0;
    background-color: ${props => props.color};
    box-shadow: 0 0 3px 3px rgba(0, 0, 0, 0.2);
    cursor: pointer;
    outline: none;
    &:focus{
        box-shadow: 0 0 8px 8px rgba(0, 0, 0, 0.4);
        outline: none;
    }
`
export default CircleButton;

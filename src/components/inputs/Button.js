import React from 'react';
import "./Button.css";
import img_qMark from '#imgs/icons/q-mark.png';
import img_gear from '#imgs/icons/gear-white.png';

const Button = ({ children, value, className, id, name, onClick, color }) => {
    if (!onClick) onClick = () => { };
    if (!name) name = "";
    if (!color) color = "bleu";

    const colors = {
        bleu: "blue",
        orange: "orange",
        vert: "vert"
    }

    return (
        <button onClick={onClick} name={name} className={`btn ${colors[color]}`}>
            {children}
        </button>
    )
}

const ButtonOrange = ({ children, value, className, id, name, onClick, color }) => {

    return (
        <button onClick={onClick} name={name} className={`btn orange`}>
            {children}
        </button>
    )
}


const ButtonYesNo = ({ onClick, name }) => {
    return (
        <button onClick={onClick} name={name} className={"btn btn-" + name.toLowerCase()}>
            {name}
        </button>
    )
}

const ButtonCircle = ({ onClick, children }) => {
    return (
        <button onClick={onClick}>
            {children}
        </button>
    )
}

const ButtonQuestionMark = ({ onClick }) => {
    return (
        <button onClick={onClick} className="btn-circle btn-question-mark">
            <img className="q-mark-img" src={img_qMark} alt="Question mark image" />
        </button>
    )
}

const ButtonSettings = ({ onClick }) => {
    return (
        <button onClick={onClick} className="btn-circle btn-blue btn-settings">
            <img className="gear-img" src={img_gear} alt="White gear" />
        </button>
    )
}

const ButtonQuestionMarkBlue = ({ onClick }) => {
    return (
        <button onClick={onClick} className="btn-circle btn-blue btn-qmark">
            <img className="q-mark-img" src={img_qMark} alt="Question mark image" />
        </button>
    )
}

const ButtonWordPick = ({ onClick, name }) => {
    const capitalized = name.substring(0, 1).toUpperCase() + name.substring(1);
    return (
        <button onClick={onClick} name={name} className="word-btn">
            {capitalized}
        </button>
    )
}

export {
    Button,
    ButtonYesNo,
    ButtonCircle,
    ButtonQuestionMark,
    ButtonSettings,
    ButtonQuestionMarkBlue,
    ButtonWordPick,
    ButtonOrange,
}
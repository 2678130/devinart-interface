import React from 'react';
import { TextInput } from './TextInput';
import "./TextBox.css";

export const TextBox = ({ onChange, onEnter, placeholder, validation, onBlur }) => {
    return (
        <div className="default-textbox">
            <TextInput onChange={onChange} validation={validation} onBlur={onBlur} placeholder={placeholder} onEnter={onEnter} />
        </div>
    )
}

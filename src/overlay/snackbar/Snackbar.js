import React, { useEffect, useState } from 'react';
import { animated, useSpring } from 'react-spring';
import styled from 'styled-components';
import Portal from '../Portal';


const SnackbarWrapper = styled(animated.div)`
    background-color: rgba(0, 0, 0, 0.5);
    position: absolute;
    top: calc(50% + ${props => props.height});
    left: 50%;
    transform: translate(-50%, -50%);
    border-radius: 5px;
    box-shadow: 0 3px 3px rgba(0, 0, 0, 0.2);
    padding: .6rem;
`

export default function Snackbar({ open, onClose, children, duration, height, parent }) {
    height = height || '-14rem';
    duration = duration || 3000;

    const [props, set] = useSpring(() => ({
        opacity: 0,
    }))

    if (open) {
        set({
            opacity: 1
        })
    }

    useEffect(() => {
        setTimeout(() => {
            if (props.opacity.getValue() === 1) {
                set({
                    opacity: 0,
                    onRest: onClose
                })
            }
        }, duration);
    }, [])


    return (
        <>
            { open &&
                <Portal parent={parent}>
                    <SnackbarWrapper style={props} height={height}>
                        {children}
                    </SnackbarWrapper>
                </Portal>
            }
        </>
    )
}
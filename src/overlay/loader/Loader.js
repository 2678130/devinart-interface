import { useEffect, useState } from 'react';
import Portal from '../Portal';
import { animated, useSpring } from 'react-spring';
import styled from 'styled-components';
import './Loader.css';

const Backdrop = styled(animated.div)`
    position: absolute;
    background-color: rgba(0, 0, 0, 0.4);
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    display: flex;
    align-items: center;
    justify-content: center;
`
const LoaderWrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
`

const Loader = ({ open, parent, children }) => {

    
    const [props, set] = useSpring(() => ({
        opacity: 0,
    }))

    if (open) {
        set({
            opacity: 1,
        })
    }
    return (
        <>
            {open &&
                <Portal parent={parent}>
                    <Backdrop style={props}>
                        <LoaderWrapper>
                            <LoaderAnimation />
                            {children}
                        </LoaderWrapper>
                    </Backdrop>
                </Portal>
            }
        </>
    );
}



const LoaderAnimation = () => {
    return (
        <div className="loader-grid"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>)
}


export default Loader;

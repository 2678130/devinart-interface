const eventNames = {
    gameJoined: "game:joined",
    gameCreated: "game:created",
    createGame: "game:create",
    joinGame: "game:join",
    guessWord: "update:guessedWord",
    guessOutcome: "update:guessOutcome",
    onDraw: "update:onDraw",
    updateDrawing: "update:drawing",
    updatePlayers: "update:players",
    getPlayers: "update:getPlayers",
    startGame: "game:start",
    gameStarted: "game:started",
    wordChosen: "update:wordChosen",
    roundStarted: "game:roundStarted",
    roundEnded: "game:roundEnded",
    gameEnded: "game:Ended"
}

export default eventNames;
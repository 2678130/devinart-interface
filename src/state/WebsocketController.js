
import { io } from 'socket.io-client';
import { pages } from '../views/pages/pages';
import evenNames from "../config/eventNames";
import Store from './store';

class WebsocketController {
    static instance;

    constructor() {
        if (WebSocket.instance) {
            return WebSocket.instance;
        }
        this.socket = undefined;
        this.eventCallbacks = new Map();
        // GAME_JOINED
        this.gameJoinedCallbacks = new Set();
        // START_GAME, GAME_STARTED
        this.gameStartedCallbacks = new Set();
        // CREATE_GAME, GAME_CREATED
        this.gameCreatedCallbacks = new Set();
        // GUESS_WORD, GUESS_OUTCOME
        this.guessOutcomeCallbacks = new Set();
        // UPDATE_DRAWING, DRAWING_UPDATED
        this.drawingUpdatedCallbacks = new Set();
        // CHOOSE_WORD, WORD_CHOSEN
        this.wordChosenCallbacks = new Set();
        // START_ROUND, ROUND_STARTED
        this.roundStartedCallbacks = new Set();
        // update players
        this.updatePlayersCallback = new Set();

        this.roundEndedCallback = new Set();

        this.path = pages.websocket.path

        WebSocket.instance = this;
    }

    connect = () => {
        if (!this.socket) {
            //this.socket = io('ws://localhost:3003');
            this.socket = io();
            this.socket.on('connect', () =>{
                Store.setState('connected', true);
            })
            this.socket.on('disconnect', () => {
                Store.setState('connected', false);
            })
            this.initializeWebsocket();
        }
    }

    getSocket = () => {
        this.connect();
        return this.socket;
    }

    emitWebsocket = (eventName, data) => {

        if (!this.socket) {
            throw new Error("Socket is not initialized");
        }

        console.log(this.socket);

        this.socket.emit(eventName, data)
    }

    registerGameJoined = (fn) => {
        this.gameJoinedCallbacks.add(fn);
    }
    registerGameStarted = (fn) => {
        this.gameStartedCallbacks.add(fn);
    }
    registerGameCreated = (fn) => {
        this.gameCreatedCallbacks.add(fn);
    }
    registerGuessOutcome = (fn) => {
        this.guessOutcomeCallbacks.add(fn);
    }
    registerDrawingUpdated = (fn) => {
        this.drawingUpdatedCallbacks.add(fn);
    }
    registerRoundStarted = (fn) => {
        this.roundStartedCallbacks.add(fn);
    }
    registerPlayerUpdated = (fn) => {
        this.updatePlayersCallback.add(fn);
    }
    registerRoundEnded = (fn) => {
        this.roundEndedCallback.add(fn);
    }

    unregisterFunction = (fn) => {
        this.gameCreatedCallbacks.delete(fn);
        this.updatePlayersCallback.delete(fn);
        this.roundStartedCallbacks.delete(fn);
        this.wordChosenCallbacks.delete(fn);
        this.gameJoinedCallbacks.delete(fn);
        this.guessOutcomeCallbacks.delete(fn);
        this.drawingUpdatedCallbacks.delete(fn);
        this.gameStartedCallbacks.delete(fn);
        this.roundEndedCallback.delete(fn);
    }

    initializeWebsocket = () => {
        // Make sure socket is initialized
        if (!this.socket) {
            this.connect();
        }
        // eventNames
        // inititalize event map
        //GAME_JOINED
        this.eventCallbacks.set(evenNames.gameJoined, this.gameJoinedCallbacks);
        // START_GAME, GAME_STARTED
        this.eventCallbacks.set(evenNames.gameStarted, this.gameStartedCallbacks);
        // CREATE_GAME, GAME_CREATED
        this.eventCallbacks.set(evenNames.gameCreated, this.gameCreatedCallbacks);
        // GUESS_WORD, GUESS_OUTCOME
        this.eventCallbacks.set(evenNames.guessOutcome, this.guessOutcomeCallbacks);
        // UPDATE_DRAWING, DRAWING_UPDATED
        this.eventCallbacks.set(evenNames.updateDrawing, this.drawingUpdatedCallbacks);
        // CHOOSE_WORD, WORD_CHOSEN
        this.eventCallbacks.set(evenNames.wordChosen, this.wordChosenCallbacks);
        // START_ROUND, ROUND_STARTED
        this.eventCallbacks.set(evenNames.roundStarted, this.roundStartedCallbacks);

        this.eventCallbacks.set(evenNames.updatePlayers, this.updatePlayersCallback);

        this.eventCallbacks.set(evenNames.roundEnded, this.roundEndedCallback);

        this.socket.onAny((event, data) => {
            const callbacks = this.eventCallbacks.get(event);
            if (!callbacks) {
                throw new Error(`Unhandled event type ${event}`);
            }

            if(event === 'disconnect') {
                Store.setState('connected', false);
            }

            callbacks.forEach(fn => {
                fn(data);
            })
        })
    }
}

export default new WebsocketController();
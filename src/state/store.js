class Store {
    static instance;
    constructor() {
        if (Store.instance) {
            return Store.instance
        }
        this.store = {};
        this.subscriptions = {};
        this.subs = new Set();
        Store.instance = this;
    }
    setState = (key, value) => {
        this.store[key] = value;
        if (!this.subscriptions[key]) {
            this.subscriptions[key] = new Set();
        }
        this.broadcast(key);
    }
    setInitialState = (key, value) => {
        if (this.store[key]) {
            return;
        }
        this.store[key] = value;
        if (!this.subscriptions[key]) {
            this.subscriptions[key] = new Set();
        }
    }
    getState = (key) => {
        if (!key) {
            return this.store;
        }
        return this.store[key];
    }
    subscribe = (fn, key = undefined) => {
        if (!key) {
            this.subs.add(fn);
        }

        if (!this.subscriptions[key]) {
            this.subscriptions[key] = new Set();
        }

        this.subscriptions[key].add(fn);
    }
    unsubscribe = (fn, key = undefined) => {
        if (!key) {
            this.subs.delete(fn);
            return;
        }
        this.subscriptions[key].delete(fn);
    }
    broadcast = (key = undefined) => {
        if (key) {
            this.subscriptions[key].forEach(fn => fn());
        }
    }
}
export default new Store();
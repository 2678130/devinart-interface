const path = require("path");

module.exports = {
    webpack: {
        alias: {
            '#hooks': path.resolve(__dirname, "src/hooks"),
            '#pages': path.resolve(__dirname, "src/views/pages"),
            '#ui': path.resolve(__dirname, "src/components"),
            '#imgs': path.resolve(__dirname, "src/imgs"),
            '#overlay': path.resolve(__dirname, "src/overlay"),
            '#components': path.resolve(__dirname, "src/views/components"),
            '#eventnames': path.resolve(__dirname, "src/config/eventNames"),
            '#gamecomponents' :path.resolve(__dirname, "src/views/pages/gamePages/components"),
            '#store': path.resolve(__dirname, "src/state/store"),
        }
    }
}